import { Routes, RouterModule } from '@angular/router';
import { BlankLayoutComponent } from './shared/layout/blank-layout/blank-layout.component';
import { AppLayoutComponent } from './shared/layout/app-layout/app-layout.component';
import { ResumeComponent } from './pages/main/resume/resume.component';


const routes: Routes = [
    {
        path: 'main',
        component: AppLayoutComponent,
        loadChildren: () => import('./pages/main/main.module').then(m => m.MainModule)
    },
    {
        path: '',
        component: BlankLayoutComponent,
        loadChildren: () => import('./pages/blank/blank.module').then(m => m.BlankModule)
    },
];


export const Routing = RouterModule.forRoot(routes);
