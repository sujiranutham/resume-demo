import { Component, OnInit } from '@angular/core';
import { EXPERIENCE } from './../../../mockdb/expereince.js';
@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.sass']
})
export class ResumeComponent implements OnInit {
  data: [] = EXPERIENCE;
  public isShow:boolean = true;
  public isShowContent:boolean = false;
  constructor() {}
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    setTimeout(()=> {
      this.isShow = false;
      this.isShowContent = true;
    },2000)
  }
}
