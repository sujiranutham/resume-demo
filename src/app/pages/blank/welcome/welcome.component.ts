import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

declare var anime: any;

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.sass'],
})
export class WelcomeComponent implements OnInit, AfterViewInit {
  constructor(private route: Router) {

  }
  ngAfterViewInit() {
    this.animateHi();
    this.animateWelcome();
    this.animateKnowMe();
    this.animatePleaseClick();
  }
  ngOnInit(): void {

  }
  gotoResume(): void {
      this.route.navigate(['/main']);
  }

  private animateHi(): void {
    // Wrap every letter in a span
    var textWrapper = document.querySelector('.hi');
    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
    anime.timeline({ loop: false })
      .add({
        targets: '.hi .letter',
        translateY: [100, 0],
        translateZ: 0,
        opacity: [0, 1],
        easing: "easeOutExpo",
        duration: 1400,
        delay: (el, i) => 300 + 30 * i
      })
  }
  private animateWelcome(): void {
    // Wrap every letter in a span
    var textWrapper = document.querySelector('.name');
    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

    anime.timeline({ loop: false })
      .add({
        targets: '.name .letter',
        translateX: [40, 0],
        translateZ: 0,
        opacity: [0, 1],
        easing: "easeOutExpo",
        duration: 3600,
        delay: (el, i) => 1000 + 60 * i
      })
  }
  private animateKnowMe(): void {
    // Wrap every letter in a span
    var textWrapper = document.querySelector('.know-me');
    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

    anime.timeline({ loop: false })
      .add({
        targets: '.know-me .letter',
        translateX: [40, 0],
        translateZ: 0,
        opacity: [0, 1],
        easing: "easeOutExpo",
        duration: 6000,
        delay: (el, i) => 1600 + 90 * i
      })
  }
  private animatePleaseClick(): void {
    anime.timeline({ loop: false })
      .add({
        targets: '.please-click .line',
        opacity: [0.5, 1],
        scaleX: [0, 1],
        easing: "easeInOutExpo",
        duration: 4100
      }).add({
        targets: '.please-click .line',
        duration: 4000,
        easing: "easeOutExpo",
        translateY: (el, i) => (-0.625 + 0.625 * 2 * i) + "em"
      }).add({
        targets: '.please-click .ampersand',
        opacity: [0, 1],
        scaleY: [0.5, 1],
        easing: "easeOutExpo",
        duration: 4000,
        offset: '-=4000'
      }).add({
        targets: '.please-click .letters-left',
        opacity: [0, 1],
        translateX: ["0.5em", 0],
        easing: "easeOutExpo",
        duration: 4000,
        offset: '-=4000'
      }).add({
        targets: '.please-click .letters-right',
        opacity: [0, 1],
        translateX: ["-0.5em", 0],
        easing: "easeOutExpo",
        duration: 4000,
        offset: '-=4000'
      })
      .add({
        targets: '.please-click .rounding-square',
        opacity: [0, 1],
        translateX: ["-0.5em", 0],
        easing: "easeOutExpo",
        duration: 3000,
        offset: '-=3000'
      })
  }
}
