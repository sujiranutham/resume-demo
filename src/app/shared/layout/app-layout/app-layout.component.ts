import { Component, OnInit, ViewChild, NgZone, AfterViewInit } from '@angular/core';
import { NgScrollbar } from 'ngx-scrollbar';
import { Subscription } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.sass']
})
export class AppLayoutComponent implements OnInit, AfterViewInit {

  @ViewChild(NgScrollbar, { static: true }) private scrollbarRef: NgScrollbar;
  private scrollSubscription = Subscription.EMPTY;
  public showGoToTop: boolean = false;
  constructor(private zone: NgZone) {

  }

  ngOnInit(): void {
    console.log("load")
  }
  ngAfterViewInit(): void {
    // Subscribe to scroll event
    this.scrollSubscription = this.scrollbarRef.scrolled.pipe(
      map((e: any) => e.target.scrollTop),
      tap((position: number) => this.zone.run(() => {
        this.showGoToTop = position > 50 ? true : false;
      }))
    ).subscribe();
  }
  gotoTop(): void {
    this.scrollbarRef.scrollTo({ top: 0 })
  }

  ngOnDestroy(): void {
    this.scrollSubscription.unsubscribe();
  }
}
