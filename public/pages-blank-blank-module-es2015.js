(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-blank-blank-module"],{

/***/ "./src/app/pages/blank/bank-routing.module.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/blank/bank-routing.module.ts ***!
  \****************************************************/
/*! exports provided: BankRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankRoutingModule", function() { return BankRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./welcome/welcome.component */ "./src/app/pages/blank/welcome/welcome.component.ts");





const routes = [
    { path: '', component: _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_2__["WelcomeComponent"] },
    { path: '**', redirectTo: '/404' }
];
class BankRoutingModule {
}
BankRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: BankRoutingModule });
BankRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function BankRoutingModule_Factory(t) { return new (t || BankRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](BankRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BankRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [],
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/pages/blank/blank.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/blank/blank.module.ts ***!
  \*********************************************/
/*! exports provided: BlankModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlankModule", function() { return BlankModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _bank_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bank-routing.module */ "./src/app/pages/blank/bank-routing.module.ts");
/* harmony import */ var _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./welcome/welcome.component */ "./src/app/pages/blank/welcome/welcome.component.ts");
/* harmony import */ var src_app_core_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/material.module */ "./src/app/core/material.module.ts");






class BlankModule {
}
BlankModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: BlankModule });
BlankModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function BlankModule_Factory(t) { return new (t || BlankModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _bank_routing_module__WEBPACK_IMPORTED_MODULE_2__["BankRoutingModule"],
            src_app_core_material_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](BlankModule, { declarations: [_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_3__["WelcomeComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _bank_routing_module__WEBPACK_IMPORTED_MODULE_2__["BankRoutingModule"],
        src_app_core_material_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BlankModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_3__["WelcomeComponent"]],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _bank_routing_module__WEBPACK_IMPORTED_MODULE_2__["BankRoutingModule"],
                    src_app_core_material_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModule"]
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/pages/blank/welcome/welcome.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/blank/welcome/welcome.component.ts ***!
  \**********************************************************/
/*! exports provided: WelcomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function() { return WelcomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class WelcomeComponent {
    constructor(route) {
        this.route = route;
    }
    ngAfterViewInit() {
        this.animateHi();
        this.animateWelcome();
        this.animateKnowMe();
        this.animatePleaseClick();
    }
    ngOnInit() {
    }
    gotoResume() {
        this.route.navigate(['/main']);
    }
    animateHi() {
        // Wrap every letter in a span
        var textWrapper = document.querySelector('.hi');
        textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
        anime.timeline({ loop: false })
            .add({
            targets: '.hi .letter',
            translateY: [100, 0],
            translateZ: 0,
            opacity: [0, 1],
            easing: "easeOutExpo",
            duration: 1400,
            delay: (el, i) => 300 + 30 * i
        });
    }
    animateWelcome() {
        // Wrap every letter in a span
        var textWrapper = document.querySelector('.name');
        textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
        anime.timeline({ loop: false })
            .add({
            targets: '.name .letter',
            translateX: [40, 0],
            translateZ: 0,
            opacity: [0, 1],
            easing: "easeOutExpo",
            duration: 3600,
            delay: (el, i) => 1000 + 60 * i
        });
    }
    animateKnowMe() {
        // Wrap every letter in a span
        var textWrapper = document.querySelector('.know-me');
        textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
        anime.timeline({ loop: false })
            .add({
            targets: '.know-me .letter',
            translateX: [40, 0],
            translateZ: 0,
            opacity: [0, 1],
            easing: "easeOutExpo",
            duration: 6000,
            delay: (el, i) => 1600 + 90 * i
        });
    }
    animatePleaseClick() {
        anime.timeline({ loop: false })
            .add({
            targets: '.please-click .line',
            opacity: [0.5, 1],
            scaleX: [0, 1],
            easing: "easeInOutExpo",
            duration: 4100
        }).add({
            targets: '.please-click .line',
            duration: 4000,
            easing: "easeOutExpo",
            translateY: (el, i) => (-0.625 + 0.625 * 2 * i) + "em"
        }).add({
            targets: '.please-click .ampersand',
            opacity: [0, 1],
            scaleY: [0.5, 1],
            easing: "easeOutExpo",
            duration: 4000,
            offset: '-=4000'
        }).add({
            targets: '.please-click .letters-left',
            opacity: [0, 1],
            translateX: ["0.5em", 0],
            easing: "easeOutExpo",
            duration: 4000,
            offset: '-=4000'
        }).add({
            targets: '.please-click .letters-right',
            opacity: [0, 1],
            translateX: ["-0.5em", 0],
            easing: "easeOutExpo",
            duration: 4000,
            offset: '-=4000'
        })
            .add({
            targets: '.please-click .rounding-square',
            opacity: [0, 1],
            translateX: ["-0.5em", 0],
            easing: "easeOutExpo",
            duration: 3000,
            offset: '-=3000'
        });
    }
}
WelcomeComponent.ɵfac = function WelcomeComponent_Factory(t) { return new (t || WelcomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"])); };
WelcomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: WelcomeComponent, selectors: [["app-welcome"]], decls: 15, vars: 0, consts: [[1, "area-welcome-text", 2, "padding-top", "20px"], [1, "hi"], [1, "name"], [1, "know-me"], [1, "please-click", 3, "click"], [1, "text-wrapper"], [1, "line", "line1", "line-top", 2, "visibility", "hidden"], [1, "letters", "letters-right"], [1, "letters", "rounding-square"], [1, "fas", "fa-circle", "spin-item"], [1, "line", "line2", "line-bottom"]], template: function WelcomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h1", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "My name is.....");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h1", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Ananchai Sujiranutham");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "For see more my information");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "h1", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WelcomeComponent_Template_h1_click_7_listener() { return ctx.gotoResume(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "span", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "span", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "please click here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "span", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "i", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["h1[_ngcontent-%COMP%] {\n  color: #fff !important;\n  margin-top: 30px;\n}\n\n\n\n.hi[_ngcontent-%COMP%] {\n  text-align: left;\n  padding: 0px 10px;\n  font-size: 2em;\n  text-transform: uppercase;\n  letter-spacing: 3px;\n  font-weight: 300;\n}\n\n.hi[_ngcontent-%COMP%]   .letter[_ngcontent-%COMP%] {\n  display: inline-block;\n  line-height: 1em;\n}\n\n\n\n.name[_ngcontent-%COMP%] {\n  margin-top: 60px;\n  text-align: left;\n  padding: 0px 15px;\n  font-weight: 200;\n  font-size: 2em;\n  text-transform: uppercase;\n  letter-spacing: 5px;\n  line-height: 1.5;\n}\n\n.name[_ngcontent-%COMP%]   .letter[_ngcontent-%COMP%] {\n  display: inline-block;\n  line-height: 1em;\n}\n\n\n\n.know-me[_ngcontent-%COMP%] {\n  padding: 0px 10px;\n  text-align: right;\n  position: absolute !important;\n  right: 0 !important;\n  bottom: 90px !important;\n  font-weight: 200;\n  font-size: 1.8em;\n  text-transform: uppercase;\n  letter-spacing: 3px;\n}\n\n.know-me[_ngcontent-%COMP%]   .letter[_ngcontent-%COMP%] {\n  display: inline-block;\n  line-height: 1em;\n}\n\n\n\n.please-click[_ngcontent-%COMP%] {\n  cursor: pointer;\n  text-align: right;\n  position: absolute !important;\n  right: 10px !important;\n  bottom: 10px !important;\n  position: relative;\n  font-weight: 300;\n  font-size: 1.8em;\n  color: #fff;\n  transition: transform 0.1s ease-in-out;\n}\n\n.please-click[_ngcontent-%COMP%]:hover {\n  transform: scale(1.02);\n}\n\n.please-click[_ngcontent-%COMP%]   .text-wrapper[_ngcontent-%COMP%] {\n  position: relative;\n  text-transform: uppercase;\n  display: inline-block;\n  padding-top: 0.1em;\n  padding-right: 0.05em;\n  padding-bottom: 0.15em;\n  line-height: 1em;\n}\n\n.please-click[_ngcontent-%COMP%]   .line[_ngcontent-%COMP%] {\n  position: absolute;\n  left: 0;\n  top: 0;\n  bottom: 0;\n  margin: auto;\n  height: 1px;\n  width: 100%;\n  background-color: #fff;\n  transform-origin: 0.5 0;\n}\n\n.line-bottom[_ngcontent-%COMP%] {\n  bottom: -5px !important;\n}\n\n.please-click[_ngcontent-%COMP%]   .ampersand[_ngcontent-%COMP%] {\n  font-family: Baskerville, serif;\n  font-style: italic;\n  font-weight: 400;\n  width: 1em;\n  margin-right: -0.1em;\n  margin-left: -0.1em;\n}\n\n.please-click[_ngcontent-%COMP%]   .letters[_ngcontent-%COMP%] {\n  display: inline-block;\n  opacity: 0;\n  padding: 15px 10px;\n}\n\n.spin-item[_ngcontent-%COMP%] {\n  -webkit-animation: sk-rotateplane 1.2s infinite ease-in-out;\n  animation: sk-rotateplane 1.2s infinite ease-in-out;\n}\n\n\n\n@media (min-width: 576px) {\n  .hi[_ngcontent-%COMP%] {\n    font-size: 3em;\n  }\n\n  .name[_ngcontent-%COMP%] {\n    font-size: 2em;\n  }\n}\n\n@media (min-width: 768px) {\n  .hi[_ngcontent-%COMP%] {\n    font-size: 3.5em;\n  }\n\n  .name[_ngcontent-%COMP%] {\n    font-size: 2.4em;\n  }\n}\n\n@media (min-width: 992px) {\n  .hi[_ngcontent-%COMP%] {\n    font-size: 4em;\n  }\n\n  .name[_ngcontent-%COMP%] {\n    font-size: 3em;\n  }\n}\n\n@media (min-width: 1200px) {\n  .hi[_ngcontent-%COMP%] {\n    font-size: 5em;\n  }\n\n  .name[_ngcontent-%COMP%] {\n    font-size: 3.5em;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmxhbmsvd2VsY29tZS9DOlxcVXNlcnNcXGFuYW5jaGFpc1xcRGVza3RvcFxcQXBwQ29kZVxccmVzdW1lXFxyZXN1bWUtZGVtby9zcmNcXGFwcFxccGFnZXNcXGJsYW5rXFx3ZWxjb21lXFx3ZWxjb21lLmNvbXBvbmVudC5zYXNzIiwic3JjL2FwcC9wYWdlcy9ibGFuay93ZWxjb21lL3dlbGNvbWUuY29tcG9uZW50LnNhc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxzQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FEQ0EsZ0JBQUE7O0FBQ0E7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ0VKOztBREFBO0VBQ0kscUJBQUE7RUFDQSxnQkFBQTtBQ0dKOztBRERBLGlCQUFBOztBQUNBO0VBQ0ksZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDSUo7O0FERkE7RUFDSSxxQkFBQTtFQUNBLGdCQUFBO0FDS0o7O0FESEEsb0JBQUE7O0FBQ0E7RUFDSSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUNNSjs7QURKQTtFQUNJLHFCQUFBO0VBQ0EsZ0JBQUE7QUNPSjs7QURMQSx5QkFBQTs7QUFFQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLDZCQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxzQ0FBQTtBQ09KOztBRExBO0VBQ0ksc0JBQUE7QUNRSjs7QUROQTtFQUNJLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0FDU0o7O0FEUEE7RUFDSSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7QUNVSjs7QURSQTtFQUNJLHVCQUFBO0FDV0o7O0FEVEE7RUFDSSwrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtBQ1lKOztBRFZBO0VBQ0kscUJBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7QUNhSjs7QURUQTtFQUNJLDJEQUFBO0VBQ0EsbURBQUE7QUNZSjs7QURSQSxxQkFBQTs7QUFFQTtFQUNJO0lBQ0ksY0FBQTtFQ1VOOztFRFJFO0lBQ0ksY0FBQTtFQ1dOO0FBQ0Y7O0FEUkE7RUFDSTtJQUNJLGdCQUFBO0VDVU47O0VEUkU7SUFDSSxnQkFBQTtFQ1dOO0FBQ0Y7O0FEVEE7RUFDSTtJQUNJLGNBQUE7RUNXTjs7RURURTtJQUNJLGNBQUE7RUNZTjtBQUNGOztBRFZBO0VBQ0k7SUFDSSxjQUFBO0VDWU47O0VEVkU7SUFDSSxnQkFBQTtFQ2FOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ibGFuay93ZWxjb21lL3dlbGNvbWUuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoMVxyXG4gICAgY29sb3I6ICNmZmYgIWltcG9ydGFudFxyXG4gICAgbWFyZ2luLXRvcDogMzBweFxyXG4gICAgXHJcbi8qIFNFQ1RJT04gSEkgfiovXHJcbi5oaSBcclxuICAgIHRleHQtYWxpZ246IGxlZnRcclxuICAgIHBhZGRpbmc6IDBweCAxMHB4XHJcbiAgICBmb250LXNpemU6IDJlbVxyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVxyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDNweFxyXG4gICAgZm9udC13ZWlnaHQ6IDMwMFxyXG5cclxuLmhpIC5sZXR0ZXIgXHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2tcclxuICAgIGxpbmUtaGVpZ2h0OiAxZW1cclxuXHJcbi8qIFNFQ1RJT04gTkFNRSAqL1xyXG4ubmFtZSBcclxuICAgIG1hcmdpbi10b3A6IDYwcHhcclxuICAgIHRleHQtYWxpZ246IGxlZnRcclxuICAgIHBhZGRpbmc6IDBweCAxNXB4XHJcbiAgICBmb250LXdlaWdodDogMjAwXHJcbiAgICBmb250LXNpemU6IDJlbVxyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVxyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDVweFxyXG4gICAgbGluZS1oZWlnaHQ6IDEuNVxyXG5cclxuLm5hbWUgLmxldHRlciBcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9ja1xyXG4gICAgbGluZS1oZWlnaHQ6IDFlbVxyXG5cclxuLyogU0VDVElPTiBLTk9XIE1FICovXHJcbi5rbm93LW1lXHJcbiAgICBwYWRkaW5nOiAwcHggMTBweFxyXG4gICAgdGV4dC1hbGlnbjogcmlnaHRcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50XHJcbiAgICByaWdodDogMCAhaW1wb3J0YW50XHJcbiAgICBib3R0b206IDkwcHggIWltcG9ydGFudFxyXG4gICAgZm9udC13ZWlnaHQ6IDIwMFxyXG4gICAgZm9udC1zaXplOiAxLjhlbVxyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVxyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDNweFxyXG5cclxuLmtub3ctbWUgLmxldHRlciBcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9ja1xyXG4gICAgbGluZS1oZWlnaHQ6IDFlbSAgXHJcblxyXG4vKiBTRUNUSU9OIFBMRUFTRSBDTElDSyAqL1xyXG4gICAgXHJcbi5wbGVhc2UtY2xpY2sgXHJcbiAgICBjdXJzb3I6IHBvaW50ZXJcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudFxyXG4gICAgcmlnaHQ6IDEwcHggIWltcG9ydGFudFxyXG4gICAgYm90dG9tOiAxMHB4ICFpbXBvcnRhbnRcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZVxyXG4gICAgZm9udC13ZWlnaHQ6IDMwMFxyXG4gICAgZm9udC1zaXplOiAxLjhlbVxyXG4gICAgY29sb3I6ICNmZmZcclxuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuMXMgZWFzZS1pbi1vdXRcclxuXHJcbi5wbGVhc2UtY2xpY2s6aG92ZXJcclxuICAgIHRyYW5zZm9ybTogc2NhbGUoMS4wMilcclxuXHJcbi5wbGVhc2UtY2xpY2sgLnRleHQtd3JhcHBlciBcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZVxyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVxyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrXHJcbiAgICBwYWRkaW5nLXRvcDogMC4xZW1cclxuICAgIHBhZGRpbmctcmlnaHQ6IDAuMDVlbVxyXG4gICAgcGFkZGluZy1ib3R0b206IDAuMTVlbVxyXG4gICAgbGluZS1oZWlnaHQ6IDFlbVxyXG5cclxuLnBsZWFzZS1jbGljayAubGluZSBcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZVxyXG4gICAgbGVmdDogMFxyXG4gICAgdG9wOiAwXHJcbiAgICBib3R0b206IDBcclxuICAgIG1hcmdpbjogYXV0b1xyXG4gICAgaGVpZ2h0OiAxcHhcclxuICAgIHdpZHRoOiAxMDAlXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmXHJcbiAgICB0cmFuc2Zvcm0tb3JpZ2luOiAwLjUgMFxyXG5cclxuLmxpbmUtYm90dG9tXHJcbiAgICBib3R0b206IC01cHggIWltcG9ydGFudFxyXG5cclxuLnBsZWFzZS1jbGljayAuYW1wZXJzYW5kIFxyXG4gICAgZm9udC1mYW1pbHk6IEJhc2tlcnZpbGxlLCBzZXJpZlxyXG4gICAgZm9udC1zdHlsZTogaXRhbGljXHJcbiAgICBmb250LXdlaWdodDogNDAwXHJcbiAgICB3aWR0aDogMWVtXHJcbiAgICBtYXJnaW4tcmlnaHQ6IC0wLjFlbVxyXG4gICAgbWFyZ2luLWxlZnQ6IC0wLjFlbVxyXG5cclxuLnBsZWFzZS1jbGljayAubGV0dGVycyBcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9ja1xyXG4gICAgb3BhY2l0eTogMFxyXG4gICAgcGFkZGluZzogMTVweCAxMHB4XHJcblxyXG5cclxuXHJcbi5zcGluLWl0ZW1cclxuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzay1yb3RhdGVwbGFuZSAxLjJzIGluZmluaXRlIGVhc2UtaW4tb3V0XHJcbiAgICBhbmltYXRpb246IHNrLXJvdGF0ZXBsYW5lIDEuMnMgaW5maW5pdGUgZWFzZS1pbi1vdXRcclxuXHJcblxyXG5cclxuLyogY29udHJvbCBmb250IHNpemUqL1xyXG4vLyBTbWFsbCBkZXZpY2VzIChsYW5kc2NhcGUgcGhvbmVzLCA1NzZweCBhbmQgdXApXHJcbkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCkgXHJcbiAgICAuaGlcclxuICAgICAgICBmb250LXNpemU6IDNlbVxyXG5cclxuICAgIC5uYW1lIFxyXG4gICAgICAgIGZvbnQtc2l6ZTogMmVtXHJcbiAgICBcclxuXHJcbi8vIE1lZGl1bSBkZXZpY2VzICh0YWJsZXRzLCA3NjhweCBhbmQgdXApXHJcbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweClcclxuICAgIC5oaVxyXG4gICAgICAgIGZvbnQtc2l6ZTogMy41ZW1cclxuXHJcbiAgICAubmFtZSBcclxuICAgICAgICBmb250LXNpemU6IDIuNGVtXHJcblxyXG4vLyBMYXJnZSBkZXZpY2VzIChkZXNrdG9wcywgOTkycHggYW5kIHVwKVxyXG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIFxyXG4gICAgLmhpXHJcbiAgICAgICAgZm9udC1zaXplOiA0ZW1cclxuXHJcbiAgICAubmFtZVxyXG4gICAgICAgIGZvbnQtc2l6ZTogM2VtXHJcblxyXG4vLyBFeHRyYSBsYXJnZSBkZXZpY2VzIChsYXJnZSBkZXNrdG9wcywgMTIwMHB4IGFuZCB1cClcclxuQG1lZGlhIChtaW4td2lkdGg6IDEyMDBweCkgXHJcbiAgICAuaGlcclxuICAgICAgICBmb250LXNpemU6IDVlbVxyXG5cclxuICAgIC5uYW1lXHJcbiAgICAgICAgZm9udC1zaXplOiAzLjVlbVxyXG5cclxuIiwiaDEge1xuICBjb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufVxuXG4vKiBTRUNUSU9OIEhJIH4qL1xuLmhpIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgcGFkZGluZzogMHB4IDEwcHg7XG4gIGZvbnQtc2l6ZTogMmVtO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBsZXR0ZXItc3BhY2luZzogM3B4O1xuICBmb250LXdlaWdodDogMzAwO1xufVxuXG4uaGkgLmxldHRlciB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbGluZS1oZWlnaHQ6IDFlbTtcbn1cblxuLyogU0VDVElPTiBOQU1FICovXG4ubmFtZSB7XG4gIG1hcmdpbi10b3A6IDYwcHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHBhZGRpbmc6IDBweCAxNXB4O1xuICBmb250LXdlaWdodDogMjAwO1xuICBmb250LXNpemU6IDJlbTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbGV0dGVyLXNwYWNpbmc6IDVweDtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbn1cblxuLm5hbWUgLmxldHRlciB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbGluZS1oZWlnaHQ6IDFlbTtcbn1cblxuLyogU0VDVElPTiBLTk9XIE1FICovXG4ua25vdy1tZSB7XG4gIHBhZGRpbmc6IDBweCAxMHB4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG4gIHJpZ2h0OiAwICFpbXBvcnRhbnQ7XG4gIGJvdHRvbTogOTBweCAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogMjAwO1xuICBmb250LXNpemU6IDEuOGVtO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBsZXR0ZXItc3BhY2luZzogM3B4O1xufVxuXG4ua25vdy1tZSAubGV0dGVyIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogMWVtO1xufVxuXG4vKiBTRUNUSU9OIFBMRUFTRSBDTElDSyAqL1xuLnBsZWFzZS1jbGljayB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICByaWdodDogMTBweCAhaW1wb3J0YW50O1xuICBib3R0b206IDEwcHggIWltcG9ydGFudDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXdlaWdodDogMzAwO1xuICBmb250LXNpemU6IDEuOGVtO1xuICBjb2xvcjogI2ZmZjtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuMXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5wbGVhc2UtY2xpY2s6aG92ZXIge1xuICB0cmFuc2Zvcm06IHNjYWxlKDEuMDIpO1xufVxuXG4ucGxlYXNlLWNsaWNrIC50ZXh0LXdyYXBwZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZy10b3A6IDAuMWVtO1xuICBwYWRkaW5nLXJpZ2h0OiAwLjA1ZW07XG4gIHBhZGRpbmctYm90dG9tOiAwLjE1ZW07XG4gIGxpbmUtaGVpZ2h0OiAxZW07XG59XG5cbi5wbGVhc2UtY2xpY2sgLmxpbmUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBtYXJnaW46IGF1dG87XG4gIGhlaWdodDogMXB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgdHJhbnNmb3JtLW9yaWdpbjogMC41IDA7XG59XG5cbi5saW5lLWJvdHRvbSB7XG4gIGJvdHRvbTogLTVweCAhaW1wb3J0YW50O1xufVxuXG4ucGxlYXNlLWNsaWNrIC5hbXBlcnNhbmQge1xuICBmb250LWZhbWlseTogQmFza2VydmlsbGUsIHNlcmlmO1xuICBmb250LXN0eWxlOiBpdGFsaWM7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHdpZHRoOiAxZW07XG4gIG1hcmdpbi1yaWdodDogLTAuMWVtO1xuICBtYXJnaW4tbGVmdDogLTAuMWVtO1xufVxuXG4ucGxlYXNlLWNsaWNrIC5sZXR0ZXJzIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBvcGFjaXR5OiAwO1xuICBwYWRkaW5nOiAxNXB4IDEwcHg7XG59XG5cbi5zcGluLWl0ZW0ge1xuICAtd2Via2l0LWFuaW1hdGlvbjogc2stcm90YXRlcGxhbmUgMS4ycyBpbmZpbml0ZSBlYXNlLWluLW91dDtcbiAgYW5pbWF0aW9uOiBzay1yb3RhdGVwbGFuZSAxLjJzIGluZmluaXRlIGVhc2UtaW4tb3V0O1xufVxuXG4vKiBjb250cm9sIGZvbnQgc2l6ZSovXG5AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpIHtcbiAgLmhpIHtcbiAgICBmb250LXNpemU6IDNlbTtcbiAgfVxuXG4gIC5uYW1lIHtcbiAgICBmb250LXNpemU6IDJlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5oaSB7XG4gICAgZm9udC1zaXplOiAzLjVlbTtcbiAgfVxuXG4gIC5uYW1lIHtcbiAgICBmb250LXNpemU6IDIuNGVtO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmhpIHtcbiAgICBmb250LXNpemU6IDRlbTtcbiAgfVxuXG4gIC5uYW1lIHtcbiAgICBmb250LXNpemU6IDNlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEyMDBweCkge1xuICAuaGkge1xuICAgIGZvbnQtc2l6ZTogNWVtO1xuICB9XG5cbiAgLm5hbWUge1xuICAgIGZvbnQtc2l6ZTogMy41ZW07XG4gIH1cbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WelcomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-welcome',
                templateUrl: './welcome.component.html',
                styleUrls: ['./welcome.component.sass'],
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }]; }, null); })();


/***/ })

}]);
//# sourceMappingURL=pages-blank-blank-module-es2015.js.map